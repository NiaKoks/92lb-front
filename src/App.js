import React,{Component, Fragment} from 'react';
import './App.css';

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Chat from "./containers/Chat/Chat";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {connect} from "react-redux";
import AddMessageForm from "./containers/AddMessageForm";
import {chatAction} from "./store/actions/messagesActions";
import {logoutUser} from "./store/actions/userActions";


class App extends Component {

    state ={
        message:''
    };

    componentDidMount() {
        if (this.props.user) {
            this.websocket = new WebSocket('ws://localhost:8000/chat?token=' + this.props.user.token);
            this.websocket.onmessage = event => {
                console.log(event);
                const decodedMessage = JSON.parse(event.data);
                this.props.chatAction(decodedMessage)
            };

            if (!this.props.user) {
                this.websocket.close();
                this.props.history.push('/login')
            }
        }
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.user && this.props.user) {
            this.websocket = new WebSocket('ws://localhost:8000/chat?token=' + this.props.user.token);
            this.websocket.onmessage = event => {
                const decodedMessage = JSON.parse(event.data);
                this.props.chatAction(decodedMessage)
            };

            if (!this.props.user) {
                this.websocket.close();
                this.props.history.push('/login')
            }
        }
    }

    submitFormHandler = event => {
        event.preventDefault();
        this.websocket.send(JSON.stringify({
            type: 'CREATE_MESSAGE',
            text: this.state.message
        }));
    };



    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        const ProtectedRoute = ({isAllowed, ...props}) => {
            return isAllowed ? <Route {...props} /> : <Redirect to="/login" />;
        };
        return (
            <Fragment>
                <header><Toolbar user={this.props.user} logout={this.props.logoutUser}/></header>
                <Switch>
                    <ProtectedRoute
                        isAllowed={this.props.user}
                        path="/" exact render={ () => <AddMessageForm
                        message={this.state.message}
                        submitFormHandler={this.submitFormHandler}
                        inputChangeHandler={this.inputChangeHandler}
                    />}
                    />
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                </Switch>
            </Fragment>
        )
    }
};

const mapStateToProps = state =>({
    user: state.users.user
});

const mapDispatchToProps = dispatch =>({
    chatAction: action =>dispatch(chatAction(action)),
    logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
