import React,{Fragment} from 'react';

const User = (props) => {
    return (
        props.users ? props.users.map((user,index)=>{
            return(
                <Fragment key={index}>
                    <div>{props.user.username}</div>
                </Fragment>
            )
        }): null
    );
};

export default User;