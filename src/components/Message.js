import React,{Fragment} from 'react';
import {Button, Card, CardBody, CardSubtitle, CardText,} from "reactstrap";

const Message = (props) => {
    return (
    props.messages ? props.messages.map((message,index)=>{
        return(
          <Fragment key={index}>
              <Card>
                  <CardBody>
                      <CardSubtitle>{props.user.username}:</CardSubtitle>
                      <CardText>{message.message}</CardText>
                      {props.user && props.user.role ==='admin' &&
                        <Button color="danger" onClick={event =>{event.stopPropagation();
                            props.delete(message._id)}}>X</Button>
                      }
                  </CardBody>
              </Card>
          </Fragment>
          )
        }) : null
    );
};

export default Message;