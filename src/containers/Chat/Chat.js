import React, {Component} from 'react';
import {connect} from "react-redux";
import AddMessageForm from "../AddMessageForm";
import {chatAction} from "../../store/actions/messagesActions";


class Chat extends Component {


    render() {
        return (
            <div>
               <AddMessageForm
                   inputChangeHandler={this.inputChangeHandler}
                   submitFormHandler={this.submitFormHandler}
                   message={this.state.message}
               />
            </div>
        );
    }
}
const mapStateToProps = state =>({
    user: state.users.user
});

const mapDispatchToProps = dispatch =>({
    chatAction: action =>dispatch(chatAction(action))
});
export default connect(mapStateToProps,mapDispatchToProps)(Chat);