import React, {Component} from 'react';
import User from "../components/User";

class OnlineUsers extends Component {
    render() {
        return (
            <div>
                <User user={this.props.user}/>
            </div>
        );
    }
}

export default OnlineUsers;