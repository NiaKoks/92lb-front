import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap"
import {connect} from "react-redux";


class AddMessageForm extends Component {


    render() {
        let chat = (
            <div>
                {this.props.messages.map((message, idx) =>(
                    <div key={idx}>
                        <b>{message.username}:</b>
                        {message.text}
                    </div>
                ))}
                <Form onSubmit={this.props.submitFormHandler}>
                    <FormGroup row>
                        <Label for="username">Username</Label>
                        <Input type="text"
                               name="message" id="message"
                               placeholder="say something"
                               value={this.props.message}
                               onChange={this.props.inputChangeHandler}
                        />
                        <Button type="submit" color="primary">Send</Button>
                    </FormGroup>
                </Form>
            </div>
            );
        return (
            <div>
                {chat}
            </div>
        );
    }
}

const mapStateToProps = state =>({
    messages:state.messages.messages,
    user: state.users.user

});




export default connect(mapStateToProps)(AddMessageForm);