import React, {Component,Fragment} from 'react';
import Message from "../components/Message";
import {connect} from "react-redux";



class AllMessages extends Component {

    componentDidMount() {

    }

    render() {
        return (
            <Fragment>
                <Message
                    messages={this.props.messages}
                    user={this.props.user}
                    delete={this.props.deleteMessages}
                />
            </Fragment>
        );
    }
}
const mapStateToProps = state =>{
  return{
      messages: state.messages.messages,
      user: state.users.user,
  }
};


export default connect(mapStateToProps)(AllMessages);