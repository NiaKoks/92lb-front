import { LATEST_MESSAGES, NEW_MESSAGE} from "../actions/messagesActions";

const initialState ={
    messages:[],
    users: [],
    error: null
};

const messagesReducer =(state =initialState, action)=>{
    switch (action.type) {
      case 'LATEST_MESSAGES':
          return {...state, messages: action.action.messages};
      case 'ACTIVE_USERS':
          return {...state, users: action.action.usernames};
      case 'NEW_MESSAGE':
          return {...state, messages: [...state.messages, action.action.message]};
      default:
          return state;
  }

};

export default messagesReducer;